SmartR
======

* R package with a set of utilities for collecting, cleaning and analysing metrics about disk volume utilsation and smart status changes.
* Since this data does not (yet) get collected systematically, the package also includes a few shell scripts based on smartctl to collect the metrics for eos and hadoop disk servers
* The input data is also used to build up a database of the current state disk configuration (to complement the existing hw inventory at initial installation time)
** goal: to track hardware changes without the need for human (and hence error prone) 

